# Generated by Django 4.2 on 2023-04-19 19:48

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="receipt",
            old_name="name",
            new_name="vendor",
        ),
    ]
